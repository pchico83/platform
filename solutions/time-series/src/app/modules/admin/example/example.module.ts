import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ExampleComponent } from 'app/modules/admin/example/example.component';
import { exampleRoutes } from 'app/modules/admin/example/example.routing';
import { SharedModule } from 'app/shared/shared.module';
import { NgApexchartsModule } from 'ng-apexcharts';
import { NgxDropzoneModule } from 'ngx-dropzone';

@NgModule({
    declarations: [
        ExampleComponent,
    ],
    imports: [
        RouterModule.forChild(exampleRoutes),
        SharedModule,

        NgApexchartsModule,
        NgxDropzoneModule,
    ]
})
export class ExampleModule {
}
