import { Component } from '@angular/core';

@Component({
    selector: 'product-data',
    templateUrl: './product-data.component.html',
    styleUrls: [
        './product-data.component.scss',
    ]
})
export class ProductDataComponent {

}
/*
{
                        title: 'Sources',
                        type: 'basic',
                    }, {
                        title: 'Streams',
                        type: 'basic',
                    }, {
                        title: 'Catalogs',
                        type: 'basic',
                    }, {
                        title: 'Visualisations',
                        type: 'basic',
                    }
*/